#!/usr/bin/env sh

wget https://s3.amazonaws.com/jsomers/dictionary.zip -O /tmp/dictionary.zip

unzip -d /tmp/dictionary /tmp/dictionary.zip

tar xjvf /tmp/dictionary/dictionary/stardict-dictd-web1913-2.4.2.tar.bz2 -C /tmp/dictionary

mv /tmp/dictionary/stardict-dictd-web1913-2.4.2/* /app/bootstrap

python convert.py
