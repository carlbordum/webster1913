FROM python:3.10

ENV WEBSTER_DB=/webster.db

RUN pip install --upgrade pip
RUN pip install flask==2.0.2 gunicorn==20.1.0

WORKDIR /app
COPY app.py app.py
COPY templates/ templates
COPY bootstrap/ bootstrap

CMD ["gunicorn", "--workers", "4", "--bind", "0.0.0.0", "--access-logfile", "-", "app:app"]
