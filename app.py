import os
import sqlite3

from flask import abort, Flask, g, redirect, render_template, request, url_for


DATABASE = os.getenv("WEBSTER_DB")


app = Flask(__name__)


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/w/<slug>")
def word_lookup(slug):
    cursor = get_db().cursor()

    # Retrieve word and meaning
    cursor.execute("select word, meaning from word where slug = ?", (slug,))
    word, meaning = cursor.fetchone()

    # Retrieve nearby words
    cursor.execute("""
      with
        cte as (
          select word, slug, row_number() over (order by word collate nocase asc) as row_number
          from word
        ),
        current as (select row_number from cte where slug = ?)
          select cte.word, cte.slug
          from cte, current
          where abs(cte.row_number - current.row_number) <= 5
          order by cte.row_number
    """, (slug,))
    nearby = cursor.fetchall()

    return render_template("word.html", word=word, meaning=meaning, nearby=nearby)


@app.route("/random")
def random():
    cursor = get_db().cursor()
    cursor.execute("select slug from word order by random() limit 1")
    slug = cursor.fetchone()
    return redirect(url_for("word_lookup", slug=slug))


@app.route("/search/")
def search():
    word = request.args.get("q")
    if word is None:
        abort(400)

    cursor = get_db().cursor()

    # If exact match is found, go to that page
    cursor.execute("select slug from word where upper(word) = upper(?)", (word,))
    slug = cursor.fetchone()
    if slug:
        return redirect(url_for("word_lookup", slug=slug))

    # Otherwise show list of possible results:
    cursor.execute("""
      select word, slug
      from search
      where word match ?
      order by rank
      limit 15
    """, (word,))
    results = cursor.fetchall()

    return render_template("search.html", word=word, results=results)
